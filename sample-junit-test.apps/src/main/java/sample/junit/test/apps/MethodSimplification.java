package sample.junit.test.apps;

public class MethodSimplification {

	int OptionOne = 0;
	int OptionTwo = 1;

	public int AMethod(boolean first, boolean second, boolean third)
	{
		if (!first)
		{
			if (second)
			{
				if (third)
				{ 
					System.out.println("Old OptionOne = " + OptionOne);
					return OptionOne;
				}
				else if (!second) // Option 2 Use Case 1
				{
					System.out.println("Old OptionTwo = " + OptionTwo);
					return OptionTwo;
				}
				else
				{
					System.out.println("Old OptionOne = " + OptionOne);
					return OptionOne;
				}
			}
			else
			{
				System.out.println("Old OptionOne = " + OptionOne);
				return OptionOne;
			}
		}
		else if (!third)
		{
			if (!second) // Option 2 Use Case 2
			{
				System.out.println("Old OptionTwo = " + OptionTwo);
				return OptionTwo;
			}
		}
		else if (!second) // Option 2 Use Case 1B
		{
			System.out.println("Old OptionTwo = " + OptionTwo);
			return OptionTwo;
		}
		if (!(third || !second)) // Option 2 Use Case 3
		{
			System.out.println("Old OptionTwo = " + OptionTwo);
			return OptionTwo;
		}
		System.out.println("Old OptionOne = " + OptionOne);
		return OptionOne;
	}
	public int simplifiedAMethod(boolean first, boolean second, boolean third) {
		if (!first) {
			if (second) {
				if (third) {
					System.out.println("New OptionOne = " + OptionOne);
					return OptionOne;
				}
				processOptionTwo(second, third); // Option 2 Use Case 1
			} else {
				System.out.println("New OptionOne = " + OptionOne);
				return OptionOne;
			}
		}
		return processOptionTwo(second, third); // Option 2 Use Case 1B, 2 && 3
	}
	private int processOptionTwo(boolean second, boolean third) {
		if ((!third && !second)) // Option 2 Use Case 3
		{
			System.out.println("New OptionTwo = " + OptionTwo);		
			return OptionTwo;
		}
		if (!(third || !second)) { // Option 2 Use Case 2
			System.out.println("New OptionTwo = " + OptionTwo);
			return OptionTwo;
		}
		if (!second) { // Option 2 Use Case 1A && 1B
			System.out.println("New OptionTwo = " + OptionTwo);
			return OptionTwo;
		}
		System.out.println("New OptionOne = " + OptionOne);
		return OptionOne;
	}
}
