package sample.junit.test.apps;
/**
 * 
 */


import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author faisal
 *
 */
public class MethodSimplificationTest {
	int OptionOne = 0;
	int OptionTwo = 1;
	MethodSimplification methodSimplification = new MethodSimplification();

    @Test
    public void simplifiedAMethodOptionOneTest() {	 
       
        System.out.println("Option One Test Case A...");
        assertEquals(OptionOne,methodSimplification.simplifiedAMethod(true,true,true));
        assertEquals(OptionOne,methodSimplification.AMethod(true,true,true));
   
        System.out.println("Option One Test Case B...");
        assertEquals(OptionOne,methodSimplification.simplifiedAMethod(false,false,false));
        assertEquals(OptionOne,methodSimplification.AMethod(false,false,false)); 
        
        System.out.println("Option One Test Case C...");
        assertEquals(OptionOne,methodSimplification.simplifiedAMethod(false,false,true));
        assertEquals(OptionOne,methodSimplification.AMethod(false,false,true));
        
        System.out.println("Option One Test Case D...");
        assertEquals(OptionOne,methodSimplification.simplifiedAMethod(false,true,true));
        assertEquals(OptionOne,methodSimplification.AMethod(false,true,true));      

        System.out.println("Option One Test Case E...");
        assertEquals(OptionOne,methodSimplification.simplifiedAMethod(true,true,true));
        assertEquals(OptionOne,methodSimplification.AMethod(true,true,true));       
        
        System.out.println("Option One Test Case F...");
        assertEquals(OptionOne,methodSimplification.simplifiedAMethod(false,true,true));
        assertEquals(OptionOne,methodSimplification.AMethod(false,true,true)); 

        System.out.println("Option One Test Case G...");
        assertEquals(OptionOne,methodSimplification.AMethod(false,true,true));
        assertEquals(OptionOne,methodSimplification.simplifiedAMethod(false,true,true));
      
        System.out.println("Option One Test Case H...");
        assertEquals(OptionOne,methodSimplification.AMethod(false,false,false));  
        assertEquals(OptionOne,methodSimplification.simplifiedAMethod(false,false,false));
           
        System.out.println("Option One Test Case I...");
        assertEquals(OptionOne,methodSimplification.simplifiedAMethod(false,false,true));
        assertEquals(OptionOne,methodSimplification.AMethod(false,false,true));
        
        System.out.println("Option Two Test Case A...");
        assertEquals(OptionTwo,methodSimplification.simplifiedAMethod(true,true,false));
        assertEquals(OptionTwo,methodSimplification.AMethod(true,true,false));
        
        System.out.println("Option Two Test Case B...");
        assertEquals(OptionTwo,methodSimplification.AMethod(true,false,false));
        assertEquals(OptionTwo,methodSimplification.simplifiedAMethod(true,false,false));
        
        System.out.println("Option Two Test Case C...");
        assertEquals(OptionTwo,methodSimplification.simplifiedAMethod(true,false,false));
        assertEquals(OptionTwo,methodSimplification.AMethod(true,false,false)); 
        
        System.out.println("Option Two Test Case D...");
        assertEquals(OptionTwo,methodSimplification.simplifiedAMethod(true,true,false));
        assertEquals(OptionTwo,methodSimplification.AMethod(true,true,false)); 
        
        System.out.println("Option Two Test Case E...");
        assertEquals(OptionTwo,methodSimplification.simplifiedAMethod(true,false,false));     
        assertEquals(OptionTwo,methodSimplification.AMethod(true,false,false)); 
    }
    @Test
    public void simplifiedAMethodOptionTwoTest() {	       

        System.out.println("Option Two Test Case A...");
        assertEquals(OptionTwo,methodSimplification.simplifiedAMethod(true,true,false));
        assertEquals(OptionTwo,methodSimplification.AMethod(true,true,false));
        
        System.out.println("Option Two Test Case B...");
        assertEquals(OptionTwo,methodSimplification.AMethod(true,false,false));
        assertEquals(OptionTwo,methodSimplification.simplifiedAMethod(true,false,false));
        
        System.out.println("Option Two Test Case C...");
        assertEquals(OptionTwo,methodSimplification.simplifiedAMethod(true,false,false));
        assertEquals(OptionTwo,methodSimplification.AMethod(true,false,false)); 
        
        System.out.println("Option Two Test Case D...");
        assertEquals(OptionTwo,methodSimplification.simplifiedAMethod(true,true,false));
        assertEquals(OptionTwo,methodSimplification.AMethod(true,true,false)); 
        
        System.out.println("Option Two Test Case E...");
        assertEquals(OptionTwo,methodSimplification.simplifiedAMethod(true,false,false));     
        assertEquals(OptionTwo,methodSimplification.AMethod(true,false,false)); 
    }
}
