/**
 * 
 */
package sample.junit.test.apps;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author faisal
 *
 */
public class StringUtilsTest {
	StringUtils sUtils = new StringUtils();
    @Test
    public void isMirrorSequencePasses() throws NotImplementedException {	 
        String s = "KAYAKS";
        assertFalse(sUtils.IsMirrorSequence(s));
    }
    @Test
    public void isMirrorSequenceFails() {
        String s = "KAYAK";
        try {
			assertTrue(sUtils.IsMirrorSequence(s));
		} catch (NotImplementedException e) {
			e.printStackTrace();
		}
    }    
}
